# tdt4250-assignment1

Submitted by: Tammy Lim 

## Description 

This project models the [5 year masters program](https://www.ntnu.no/studier/mtdt/studieretninger-og-hovedprofiler#year=2022&programmeCode=MTDT&dir=) in Computer Technology by NTNU. 
  
## Features of Ecore used

### 1. Classes 

`Department`: The root container which contains all the available courses and programmes.

`Programme`: A programme represents a specific study programme such as Masters in Computer Science and contains multiple semesters necessary as presented in the study plan. 

`Courses`: A course contains the details of a course such as the name, code, number of credits and so on. For example, TDT4250 Advanced Software Design is a course that is available in the Autumn season and has 7.5 credits. 

`Semester`:  A semester represents several courses that are allocated to each semester as presented by the study plan. The semester number indicates a particular semester of the study plan. For example in the first year, we have semester 1 for the autumn season and semester 2 for the spring season. 

`Specialisations`: A programme has one or more possible specialisations to choose from. For example, Artificial Intelligence or Algorithms are possible specialisations for the Masters in Computer Science programme.

### 2. Datatypes: Enums

`Season`: Only two seasons are available: Spring and Autumn.

`Courselevel`: Following NTNU's terminology, we have the following course levels: foundation, intermediate, third year, second degree and doctoral degree. 

### 3. Constraints

As part of the requirements, both manual and OCL constraints were added to the model. 

| Manual: max30Credits | OCL: atMost15Credits |
| ------ | ------ |
| The maximum number of credits that can be taken in 1 semester is 30.0 | A course can provide 0 to 15.0 credits |

### 4. Derived Features

The `credits` attribute under `semester` is derived from the sum of all the courses taken in a semester. 

## Model Diagram

![alt text](/Screenshot_2022-09-23_at_14.56.32.png "Title Text")


## Model Instance

![alt text](/Screenshot_2022-09-23_at_15.14.40.png "Title Text")

