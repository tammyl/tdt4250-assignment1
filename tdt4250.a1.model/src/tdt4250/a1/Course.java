/**
 */
package tdt4250.a1;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.a1.Course#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.a1.Course#getCode <em>Code</em>}</li>
 *   <li>{@link tdt4250.a1.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.a1.Course#getSeasonType <em>Season Type</em>}</li>
 *   <li>{@link tdt4250.a1.Course#getLevel <em>Level</em>}</li>
 * </ul>
 *
 * @see tdt4250.a1.A1Package#getCourse()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='atMost15Credits'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 atMost15Credits='self.credits &lt;= 15.0'"
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.a1.A1Package#getCourse_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.a1.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Code</em>' attribute.
	 * @see #setCode(String)
	 * @see tdt4250.a1.A1Package#getCourse_Code()
	 * @model required="true"
	 * @generated
	 */
	String getCode();

	/**
	 * Sets the value of the '{@link tdt4250.a1.Course#getCode <em>Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Code</em>' attribute.
	 * @see #getCode()
	 * @generated
	 */
	void setCode(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(float)
	 * @see tdt4250.a1.A1Package#getCourse_Credits()
	 * @model required="true"
	 * @generated
	 */
	float getCredits();

	/**
	 * Sets the value of the '{@link tdt4250.a1.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(float value);

	/**
	 * Returns the value of the '<em><b>Season Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.a1.Season}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Season Type</em>' attribute.
	 * @see tdt4250.a1.Season
	 * @see #setSeasonType(Season)
	 * @see tdt4250.a1.A1Package#getCourse_SeasonType()
	 * @model
	 * @generated
	 */
	Season getSeasonType();

	/**
	 * Sets the value of the '{@link tdt4250.a1.Course#getSeasonType <em>Season Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Season Type</em>' attribute.
	 * @see tdt4250.a1.Season
	 * @see #getSeasonType()
	 * @generated
	 */
	void setSeasonType(Season value);

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.a1.CourseLevel}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see tdt4250.a1.CourseLevel
	 * @see #setLevel(CourseLevel)
	 * @see tdt4250.a1.A1Package#getCourse_Level()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	CourseLevel getLevel();

	/**
	 * Sets the value of the '{@link tdt4250.a1.Course#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see tdt4250.a1.CourseLevel
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(CourseLevel value);

} // Course
