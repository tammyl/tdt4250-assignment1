/**
 */
package tdt4250.a1;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>semester</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.a1.semester#getNumber <em>Number</em>}</li>
 *   <li>{@link tdt4250.a1.semester#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.a1.semester#getSeasonType <em>Season Type</em>}</li>
 *   <li>{@link tdt4250.a1.semester#getCourses <em>Courses</em>}</li>
 * </ul>
 *
 * @see tdt4250.a1.A1Package#getsemester()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='max30Credits'"
 * @generated
 */
public interface semester extends EObject {
	/**
	 * Returns the value of the '<em><b>Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number</em>' attribute.
	 * @see #setNumber(int)
	 * @see tdt4250.a1.A1Package#getsemester_Number()
	 * @model required="true"
	 * @generated
	 */
	int getNumber();

	/**
	 * Sets the value of the '{@link tdt4250.a1.semester#getNumber <em>Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number</em>' attribute.
	 * @see #getNumber()
	 * @generated
	 */
	void setNumber(int value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see tdt4250.a1.A1Package#getsemester_Credits()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	float getCredits();

	/**
	 * Returns the value of the '<em><b>Season Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.a1.Season}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Season Type</em>' attribute.
	 * @see tdt4250.a1.Season
	 * @see #setSeasonType(Season)
	 * @see tdt4250.a1.A1Package#getsemester_SeasonType()
	 * @model required="true"
	 * @generated
	 */
	Season getSeasonType();

	/**
	 * Sets the value of the '{@link tdt4250.a1.semester#getSeasonType <em>Season Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Season Type</em>' attribute.
	 * @see tdt4250.a1.Season
	 * @see #getSeasonType()
	 * @generated
	 */
	void setSeasonType(Season value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.a1.Course}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' containment reference list.
	 * @see tdt4250.a1.A1Package#getsemester_Courses()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Course> getCourses();

} // semester
